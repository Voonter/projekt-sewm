function [ P_permute, enc_payload ] = payload_encryption(payload, permkey, randkey)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

s = size(payload);

rng(permkey,'twister'); %Inicjalizuje seed generatora dla permutacji
V_per_hor = randperm(s(2));

rng(permkey,'twister');
V_per_ver = randperm(s(1));



for i=1:s(1) %permutacja w poziomie 
    for j=1:s(2)
        P_permute_hor(i,j) = payload(i,V_per_hor(1,j));
    end
end

for i=1:s(2) %permutacja w pionie, wszystko na podstawie jednego seeda z randperma.
    for j=1:s(1)
        P_permute(j,i) = P_permute_hor(V_per_ver(1,j),i);
    end
end

rng(randkey,'twister'); %Inicjalizuje seed generatora
Rand = randi([0 1],s(1),s(2),'uint8'); %wygenerowana maciez losowych watrosci
Rand = logical(Rand); 

enc_payload = bitxor(Rand,P_permute); 

end

