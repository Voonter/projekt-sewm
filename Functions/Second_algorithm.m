function varargout = Second_algorithm(varargin)
% SECOND_ALGORITHM MATLAB code for Second_algorithm.fig
%      SECOND_ALGORITHM, by itself, creates a new SECOND_ALGORITHM or raises the existing
%      singleton*.
%
%      H = SECOND_ALGORITHM returns the handle to a new SECOND_ALGORITHM or the handle to
%      the existing singleton*.
%
%      SECOND_ALGORITHM('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SECOND_ALGORITHM.M with the given input arguments.
%
%      SECOND_ALGORITHM('Property','Value',...) creates a new SECOND_ALGORITHM or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Second_algorithm_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Second_algorithm_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Second_algorithm

% Last Modified by GUIDE v2.5 18-Jun-2014 23:26:55

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Second_algorithm_OpeningFcn, ...
                   'gui_OutputFcn',  @Second_algorithm_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Second_algorithm is made visible.
function Second_algorithm_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Second_algorithm (see VARARGIN)

% Choose default command line output for Second_algorithm
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Second_algorithm wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Second_algorithm_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

set(handles.qimStepMenuConstr,'Enable','off');
set(handles.randSeedConstr,'Enable','off');
set(handles.perm_seed_constr,'Enable','off');
set(handles.Scalar_construct,'Enable','off');
set(handles.createImageButton,'Enable','off');
set(handles.originalWatermarkedButton,'Enable','off');
set(handles.saveWatermarkedImageButton,'Enable','off');

set(handles.qimStepMenuReconstr,'Enable','off');
set(handles.randSeedReconstr,'Enable','off');
set(handles.perm_seed_consruct,'Enable','off');
set(handles.Scalar_reconstruct,'Enable','off');
set(handles.showOriginalReconstructedImage,'Enable','off');
set(handles.saveReconstructedImageButton,'Enable','off');
set(handles.restoreImageButton,'Enable','off');


% --- Executes on selection change in qimStepMenuReconstr.
function qimStepMenuReconstr_Callback(hObject, eventdata, handles)
% hObject    handle to qimStepMenuReconstr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns qimStepMenuReconstr contents as cell array
%        contents{get(hObject,'Value')} returns selected item from qimStepMenuReconstr


% --- Executes during object creation, after setting all properties.
function qimStepMenuReconstr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to qimStepMenuReconstr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- RAND SEED RECONSTR RIGHT SIDE 
function randSeedReconstr_Callback(hObject, eventdata, handles)
	rseed = str2double(get(handles.randSeedReconstr,'String'));
	if isnan(rseed) == 1
		set(handles.randSeedReconstr,'String','123455');
	end

% --- Executes during object creation, after setting all properties.
function randSeedReconstr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to randSeedReconstr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- PERM SEED RECONSTR RIGHT SIDE
function perm_seed_consruct_Callback(hObject, eventdata, handles)
	perm_seed = str2double(get(handles.perm_seed_consruct,'String'));
	if isnan(perm_seed) || perm_seed < 0
		set(handles.perm_seed_consruct,'String','54321');
	end	

%
function perm_seed_consruct_CreateFcn(hObject, eventdata, handles)
% hObject    handle to perm_seed_consruct (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- SCALAR RECONSTR RIGHT SIDE
function Scalar_reconstruct_Callback(hObject, eventdata, handles)
	scalar = str2double(get(handles.Scalar_reconstruct,'String'));
	if isnan(scalar) || scalar < 0 || scalar > 1
		set(handles.Scalar_reconstruct,'String','0.13');
	end	


% --- Executes during object creation, after setting all properties.
function Scalar_reconstruct_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Scalar_reconstruct (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in qimStepMenuConstr.
function qimStepMenuConstr_Callback(hObject, eventdata, handles)
% hObject    handle to qimStepMenuConstr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns qimStepMenuConstr contents as cell array
%        contents{get(hObject,'Value')} returns selected item from qimStepMenuConstr


% --- Executes during object creation, after setting all properties.
function qimStepMenuConstr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to qimStepMenuConstr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- RAND SEED CONSTRUCT CALLBACK
function randSeedConstr_Callback(hObject, eventdata, handles)
	randSeed = str2double(get(handles.randSeedConstr,'String'));
	if isnan(randSeed)
		set(handles.randSeedConstr,'String','123455');
	end

% --- Executes during object creation, after setting all properties.
function randSeedConstr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to randSeedConstr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- PERM SEED CONSTRUCT CALLBACK LEFT SIDE
function perm_seed_constr_Callback(hObject, eventdata, handles)
	perm_seed = str2double(get(handles.perm_seed_constr,'String'));
	if isnan(perm_seed) || perm_seed < 0
		set(handles.perm_seed_constr,'String','54321');
	end

% --- Executes during object creation, after setting all properties.
function perm_seed_constr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to perm_seed_constr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- SCALAR CONSTRUCT CALLBACK LEFT SIDE
function Scalar_construct_Callback(hObject, eventdata, handles)
	scalar = str2double(get(handles.Scalar_construct,'String'));
	if isnan(scalar) || scalar < 0 || scalar > 1
		set(handles.Scalar_construct,'String','0.13');
	end

% --- Executes during object creation, after setting all properties.
function Scalar_construct_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Scalar_construct (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- CREATE WATERMARKED IMAGE CALLBACK LEFT SIDE
function createImageButton_Callback(hObject, eventdata, handles)
	X = getappdata(handles.createImageButton, 'Original');
	rand_seed = int32(str2double(get(handles.randSeedConstr,'String')));
	scalar = str2double(get(handles.Scalar_construct,'String'));
	perm_seed = str2double(get(handles.perm_seed_constr,'String'));
	qimString = get(handles.qimStepMenuConstr, 'String');
    qimIndex = get(handles.qimStepMenuConstr, 'Value');
    qim = str2double(qimString(qimIndex));
	
	Y = watermarking(X,rand_seed,perm_seed,scalar,qim);
	setappdata(handles.createImageButton, 'Watermarked', Y);
	
	
	set(handles.image_created, 'String', 'Image watermarked');
	set(handles.originalWatermarkedButton,'Enable','on');
	set(handles.saveWatermarkedImageButton,'Enable','on');

	
	
% --- COMPARE ORIGINAL AND WATERMARKED IMAGE LEFT SIDE
function originalWatermarkedButton_Callback(hObject, eventdata, handles)
	X = getappdata(handles.createImageButton,'Original');
	Y = getappdata(handles.createImageButton,'Watermarked');
	[ psnr ] = measerr(X,Y);
	figure('ToolBar','figure');
	colormap gray;
	subplot(121)
	imagesc(X); title('Original image'); axis equal; axis tight; colormap gray;
	subplot(122)
	imagesc(Y); title('Watermarked image'); axis equal; axis tight; colormap gray;
	PSNRBox = uicontrol('style','text');
	PSNRBoxvalue = uicontrol('style','text');
	set(PSNRBox,'String','PSNR: ');
	set(PSNRBoxvalue,'String',psnr);
	set(PSNRBox,'Position',[200,30,50,20]);
	set(PSNRBoxvalue,'Position',[250,30,50,20]);
	
	
% --- COMPARE WATERMARKED AND RECONSTRUCTED IMAGE CALLBACK - RIGHT SIDE
function showOriginalReconstructedImage_Callback(hObject, eventdata, handles)
	X = getappdata(handles.restoreImageButton, 'Watermarked');
	Y = getappdata(handles.restoreImageButton,'Restored');
	[ psnr ] = measerr(X,Y);
	figure('ToolBar','figure');
	colormap gray;
	subplot(121)
	imagesc(X); title('Tampered image'); axis equal; axis tight; colormap gray;
	subplot(122)
	imagesc(Y); title('Reconstructed image'); axis equal; axis tight; colormap gray;
	PSNRBox = uicontrol('style','text');
	PSNRBoxvalue = uicontrol('style','text');
	set(PSNRBox,'String','PSNR: ');
	set(PSNRBoxvalue,'String',psnr);
	set(PSNRBox,'Position',[200,30,50,20]);
	set(PSNRBoxvalue,'Position',[250,30,50,20]);
	

% --- SAVE RECONSTRUCTED IMAGE BUTTON CALLBACK RIGHT SIDE
function saveReconstructedImageButton_Callback(hObject, eventdata, handles)
	[FileName,PathName,FilterIndex] = uiputfile({'*.png';'*.jpg'},'Save restored file');
	Y = uint8(getappdata(handles.restoreImageButton,'Restored'));
	if FilterIndex == 1
         imwrite(Y,[PathName FileName],'png');
    elseif FilterIndex == 2
         imwrite(Y,[PathName FileName],'jpg','Quality',100);
    end


% --- SAVE WATERMARKED IMAGE BUTTON CALLBACK LEFT SIDE
function saveWatermarkedImageButton_Callback(hObject, eventdata, handles)
	[FileName,PathName,FilterIndex] = uiputfile({'*.png';'*.jpg'},'Save watermarked file');
	Y = uint8(getappdata(handles.createImageButton,'Watermarked'));
	if FilterIndex == 1
         imwrite(Y,[PathName FileName],'png');
    elseif FilterIndex == 2
         imwrite(Y,[PathName FileName],'jpg','Quality',100);
	else
		 imwrite(Y,[PathName FileName],'png');
    end



% --- LOAD IMAGE BUTTON CALLBACK - LEFT SIDE
function loadImageButton_Callback(hObject, eventdata, handles)

	[FileName,PathName] = uigetfile({'*.png;*.jpg'},'Select file');
	if FileName ~= 0
		
		X = imread([PathName FileName]);
		if size(X,3) > 1
			X = rgb2gray(X); 
		end
		if mod(size(X,1),8) ~= 0 || mod(size(X,2),8) ~= 0
			h = msgbox('Wrong image size','Error');																		
		else
			set(handles.qimStepMenuConstr,'Enable','on');
			set(handles.randSeedConstr,'Enable','on');
			set(handles.perm_seed_constr,'Enable','on');
			set(handles.Scalar_construct,'Enable','on');
			set(handles.createImageButton,'Enable','on');
			
			setappdata(handles.originalWatermarkedButton ,'OriginalImage',X)
			setappdata(handles.createImageButton, 'Original', X);
 
			set(handles.status_zaladowania_oryginalu, 'String', strcat('Image loaded: ',FileName)); 
			set(handles.image_created,'String','Image not created');
		end
	end


% --- LOAD WATERMARKED IMAGE BUTTON CALLBACK - RIGHT SIDE
function loadWatermarkedImageButton_Callback(hObject, eventdata, handles)
	[FileName,PathName] = uigetfile({'*.png;*.jpg'},'Select file');
	if FileName ~= 0
		
		X = imread([PathName FileName]);
		if size(X,3) > 1
			X = rgb2gray(X);
		end
		if mod(size(X,1),8) ~= 0 || mod(size(X,2),8) ~= 0
			h = msgbox('Wrong image size','Error');																	
		else
			set(handles.qimStepMenuReconstr,'Enable','on');
			set(handles.randSeedReconstr,'Enable','on');
			set(handles.perm_seed_consruct,'Enable','on');
			set(handles.Scalar_reconstruct,'Enable','on');
			set(handles.restoreImageButton,'Enable','on');
			
			setappdata(handles.restoreImageButton, 'Watermarked', X);
 
			set(handles.text16, 'String', strcat('Image loaded: ',FileName)); 
			set(handles.reconstruction_text,'String','Image not restored');
			
		end
	end
	


% --- RESTORE IMAGE BUTTON CALLBACK - RIGHT SIDE
function restoreImageButton_Callback(hObject, eventdata, handles)
	X = getappdata(handles.restoreImageButton, 'Watermarked');
	rand_seed = str2double(get(handles.randSeedReconstr,'String'));
	perm_seed = str2double(get(handles.perm_seed_consruct,'String'));
	scalar = str2double(get(handles.Scalar_reconstruct,'String'));
	qimString = get(handles.qimStepMenuReconstr, 'String');
    qimIndex = get(handles.qimStepMenuReconstr, 'Value');
    qim = str2double(qimString(qimIndex));
	
	Y = rekonstrukcja(X,rand_seed,perm_seed,scalar,qim);
	
	setappdata(handles.restoreImageButton, 'Restored', Y);
	
	set(handles.reconstruction_text, 'String', 'Image restored');
	set(handles.showOriginalReconstructedImage,'Enable','on');
	set(handles.saveReconstructedImageButton,'Enable','on');
	
