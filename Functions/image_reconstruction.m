function [ image ] = image_reconstruction( ext_payload, image_size )

I=uint8(ext_payload);
[r,c] = find(I); 
for i=1:size(r)
    I(r(i),c(i)) = 255;
end
H = fspecial('gaussian', [5 5], 2);
F = imfilter(I, H, 'same');

image = imresize(F, [image_size(1) image_size(2)]);

end

