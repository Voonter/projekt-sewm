function [ ext_payload ] = payload_extraction( watermarkedimage, delta )
%	Funkcja pobiera oznakowany obrazek oraz wartosc kroku kwantyzacji.
% 	Zwraca odzyskany payload.

[cA,cH,cV,cD] = dwt2(watermarkedimage,'haar','mode','sym');

s = size(cA);
MAX = max(cA(:));

zeros=0:2*delta:MAX+delta;
ones=0+delta:2*delta:MAX;

for i=1:s(1)
    for j=1:s(2)
        w(1)=min(abs(cA(i,j)-ones));
        w(2)=min(abs(cA(i,j)-zeros));
        

       if w(1)<w(2)
            ext_payload(i,j)=1;
        else
            ext_payload(i,j)=0;
        end
    end
end

ext_payload = logical(ext_payload);

end

