function [ Y ] = rekonstrukcja( X , seed, perm_seed, scalar, qim)


X = double(X);

N = size(X,1);
M = size(X,2);

step = 8;

Y = zeros(N,M);
Z = zeros(8,8);
U = zeros(N,M);

a1 = 2;
a2 = 2.2;
a3 = 2.2;
a4 = 2.1;

RandX = GenerateRandX(seed);

for i=1:step:N
    for j=1:step:M
        U(i:i+step-1,j:j+step-1) = dct2(X(i:i+step-1,j:j+step-1));
    end
end

tempX = zeros(4,1);

[ X_loc Y_loc ] = coefficients_location(perm_seed,N/step,M/step);

for i=1:step:N
    for j=1:step:M
      il = 0;
      q = 1;
      for k = 1:step
           for l = 1:step                 
              if RandX(k,l) == 2
                 posX = (X_loc(floor(i/step)+1) - 1) * step + k;
                 posY = (Y_loc(floor(j/step)+1) - 1) * step + l;
				 tempX(q) = U(posX,posY)/scalar;
                 q = q + 1;
              else
                 a = mod(U(i+k-1,j+l-1),2*qim);
                 if a > qim/2 && a < 3*qim/2
                    il = il + 1;
                 end
              end
           end
      end
	  if il > 6
 		Z(1,1) = a1*(sum(tempX));
		Z(1,2) = a2*(tempX(1) - tempX(2) + tempX(3) - tempX(4));
		Z(2,1) = a3*(tempX(1) + tempX(2) - tempX(3) - tempX(4));
		Z(2,2) = a4*(tempX(1) - tempX(2) - tempX(3) + tempX(4));
		Z = idct2(Z);
	  else
		Z = U(i:i+step-1,j:j+step-1);
		Z = idct2(Z);
	  end
	  Y(i:i+step-1,j:j+step-1) = Z;
	  Z(:,:) = 0;
    end
end

Y = uint8(Y);

end

