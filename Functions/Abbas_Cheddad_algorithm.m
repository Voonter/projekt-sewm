function varargout = Abbas_Cheddad_algorithm(varargin)
% PROGRAM1 MATLAB code for Program1.fig
%      ABBAS_CHEDDAD_ALGORITHM, by itself, creates a new PROGRAM1 or raises the existing
%      singleton*.
%
%      H = PROGRAM1 returns the handle to a new PROGRAM1 or the handle to
%      the existing singleton*.
%
%      PROGRAM1('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ABBAS_CHEDDAD_ALGORITHM.M with the given input arguments.
%
%      PROGRAM1('Property','Value',...) creates a new PROGRAM1 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Program1_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Program1_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Abbas_Cheddad_algorithm

% Last Modified by GUIDE v2.5 20-Jun-2014 18:25:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Abbas_Cheddad_algorithm_OpeningFcn, ...
                   'gui_OutputFcn',  @Abbas_Cheddad_algorithm_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Abbas_Cheddad_algorithm is made visible.
function Abbas_Cheddad_algorithm_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Abbas_Cheddad_algorithm (see VARARGIN)

% Choose default command line output for Abbas_Cheddad_algorithm
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Abbas_Cheddad_algorithm wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Abbas_Cheddad_algorithm_OutputFcn(~, ~, handles) 

varargout{1} = handles.output;

set(handles.qim_choice,'Enable','off'); 
set(handles.rand_choice,'Enable','off'); 
set(handles.rand_choice,'String','940583'); 
set(handles.perm_choice,'String','305748'); 
set(handles.perm_choice,'Enable','off');
set(handles.drand_choice,'String','940583'); 
set(handles.dperm_choice,'String','305748'); 
set(handles.dqim_choice,'Enable','off'); 
set(handles.drand_choice,'Enable','off'); 
set(handles.dperm_choice,'Enable','off'); 

set(handles.SaveImg_button,'Enable','off'); 
set(handles.SavePayload_button,'Enable','off'); 
set(handles.SaveExtPayload_button,'Enable','off'); 
set(handles.createImg_button,'Enable','off'); 
set(handles.extractImg_button,'Enable','off'); 
set(handles.compareImg_button,'Enable','off');
set(handles.showPay_button,'Enable','off'); 
set(handles.showImg_button,'Enable','off'); 
set(handles.showRecPay_button,'Enable','off'); 
set(handles.saveImage_button,'Enable','off'); 

set(handles.create_text, 'String', 'Image not created'); 
set(handles.loadImg_text, 'String', 'Image not loaded');
set(handles.loadTampImg_text, 'String', 'Image not loaded');
set(handles.extract_text, 'String', 'Payload not extracted'); 



%% ---------- IMAGE LOAD BUTTON ----------
function loadImg_button_Callback(~, ~, handles)

[FileName,PathName] = uigetfile({'*.png;*.jpg'},'Select file');

if FileName ~= 0

    X = imread([PathName FileName]); %Wczytuje obrazek 
    if size(X,3)>1
        X=rgb2gray(X);
    end
    
    S = size(X);
    XX = {X,S};
    %setappdata(handles.compareImg_button ,'OriginalImage',XX)
    setappdata(handles.createImg_button, 'Original', X);
    
    set(handles.loadImg_text, 'String', 'Image loaded'); 
    set(handles.imageName_text, 'String', FileName);
    set(handles.qim_choice,'Enable','on'); 
    set(handles.rand_choice,'Enable','on'); 
    set(handles.perm_choice,'Enable','on'); 
    set(handles.createImg_button,'Enable','on');
    set(handles.SavePayload_button,'Enable','off'); 
    set(handles.showPay_button,'Enable','off'); 
    set(handles.showImg_button,'Enable','off'); 
    set(handles.create_text, 'String', 'Image not created'); 
else
    set(handles.create_text, 'String', 'Image created'); 
end
 

%% ---------- QIM, RAND SEED, PERM SEED CHOICE WIDGETS ----------
function qim_choice_Callback(~, ~, handles)

set(handles.create_text, 'String', 'Image not created');

% --- Executes during object creation, after setting all properties.
function qim_choice_CreateFcn(hObject, ~, ~)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function rand_choice_Callback(~, ~, handles)

set(handles.create_text, 'String', 'Image not created');

% --- Executes during object creation, after setting all properties.
function rand_choice_CreateFcn(hObject, ~, ~)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function perm_choice_Callback(~, ~, handles)

set(handles.create_text, 'String', 'Image not created');

% --- Executes during object creation, after setting all properties.
function perm_choice_CreateFcn(hObject, ~, ~)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%% ---------- WATERMARKED IMAGE CREATE BUTTON ----------
function createImg_button_Callback(~, ~, handles)


a = str2double(get(handles.rand_choice,'String'));
b = str2double(get(handles.perm_choice,'String'));

if isnan(a) || isnan(b) || a > intmax('uint32') || b > intmax('uint32') || a < 0 || b < 0
    msgbox('Embedding values are not proper!', 'Error','error'); 
else 
    X = getappdata(handles.createImg_button, 'Original');

    rand_seed = int32(a);
    perm_seed = int32(b);
    d = get(handles.qim_choice, 'String');
    v = get(handles.qim_choice, 'Value');
    delta = str2double(d(v));

    P = payload_construction(X);
    [P_per, P_enc] = payload_encryption(P, perm_seed, rand_seed);
    Y = payload_embedding(X,P_enc, delta);
    
    [PSNR] = measerr(X,Y);  
    C1 = {P, P_per, P_enc};
    C2 = {X, Y, PSNR};  
    
    set(handles.create_text, 'String', 'Image created');
    
    setappdata(handles.showPay_button,'ImageEncryptionProcess',C1);
    setappdata(handles.SavePayload_button,'Payload',P);  
    setappdata(handles.showImg_button,'OriginalAndWatermarkedImage',C2); 
    setappdata(handles.SaveImg_button,'Image', Y);
    
    set(handles.SaveImg_button,'Enable','on'); 
    set(handles.SavePayload_button,'Enable','on'); 
    set(handles.showPay_button,'Enable','on'); 
    set(handles.showImg_button,'Enable','on');    
end


%% ---------- SHOW PAYLOAD ENCRYPTION PROCESS BUTTON ----------
function showPay_button_Callback(~, ~, handles)

C = getappdata(handles.showPay_button,'ImageEncryptionProcess');
P = C{1,1};
P_per = C{1,2};
P_enc = C{1,3};

figure('ToolBar','figure');
colormap gray;
subplot(221)
imagesc(P); title('Payload'); axis equal; axis tight;
subplot(222)
imagesc(P_per); title('Permuted payload'); axis equal; axis tight;
subplot(223)
imagesc(P_enc); title('Permuted payload XOR-ed with pseudorandom map'); axis equal; axis tight;


%% ---------- SHOW ORIGINAL AND WATERMARKED BUTTON ----------
function showImg_button_Callback(~, ~, handles)

C = getappdata(handles.showImg_button,'OriginalAndWatermarkedImage');
X = C{1,1};
Y = C{1,2};
PSNR = C{1,3};

figure('ToolBar','figure');

colormap gray;
subplot(121)
imagesc(X); title('Original image'); axis equal; axis tight; colormap gray;
subplot(122)
imagesc(Y); title('Watermarked image'); axis equal; axis tight; colormap gray;
PSNRBox = uicontrol('style','text');
PSNRBoxvalue = uicontrol('style','text');
set(PSNRBox,'String','PSNR: ');
set(PSNRBoxvalue,'String',PSNR);
set(PSNRBox,'Position',[200,30,50,20]);
set(PSNRBoxvalue,'Position',[250,30,50,20]);

%% ---------- SAVE PAYLOAD BUTTON ----------
function SavePayload_button_Callback(hObject, eventdata, handles)

S = getappdata(handles.SavePayload_button,'Payload');

[FileName,PathName,FilterIndex] = uiputfile({'*.png';'*.jpg'},'Save extracted payload');
 if FilterIndex == 1
         imwrite(S,[PathName FileName],'png');
    elseif FilterIndex == 2
         imwrite(S,[PathName FileName],'jpg','Mode','lossless');
 end


%% ---------- SAVE IMAGE BUTTON ----------
function SaveImg_button_Callback(hObject, eventdata, handles)

S = getappdata(handles.SaveImg_button,'Image');

[FileName,PathName,FilterIndex] = uiputfile({'*.png';'*.jpg'},'Save watermarked file');
if FilterIndex == 1
     imwrite(S,[PathName FileName],'png');
elseif FilterIndex == 2
     imwrite(S,[PathName FileName],'jpg','Mode','lossless');
end
 

%% ---------- TAMPERED IMAGE LOAD BUTTON ----------
function loadTampImg_button_Callback(~, ~, handles)

[FileName,PathName] = uigetfile({'*.png;*.jpg'},'Select .png file');

if FileName ~= 0

    X = imread([PathName FileName]); %Wczytuje obrazek 
    if size(X,3)>1
        X=rgb2gray(X);
    end
    setappdata(handles.extractImg_button, 'TamperedImage', X);
    set(handles.tempimageName_text, 'String', FileName);

    set(handles.SaveExtPayload_button,'Enable','off'); 
    set(handles.extractImg_button,'Enable','on');
    set(handles.dqim_choice,'Enable','on'); 
    set(handles.drand_choice,'Enable','on'); 
    set(handles.dperm_choice,'Enable','on'); 
    set(handles.showRecPay_button,'Enable','off'); 
    set(handles.compareImg_button,'Enable','off');
    set(handles.saveImage_button,'Enable','off'); 
    set(handles.loadTampImg_text, 'String', 'Image loaded');
    set(handles.extract_text, 'String', 'Payload not extracted');
else
    set(handles.extract_text, 'String', 'Payload extracted');
end


%% ---------- QIM, RAND SEED, PERM SEED CHOICE FOR EXTRACTING WATERMAK WIDGETS ----------
function dqim_choice_Callback(~, ~, handles)

set(handles.extract_text, 'String', 'Payload not extracted');

% --- Executes during object creation, after setting all properties.
function dqim_choice_CreateFcn(hObject, ~, ~)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function drand_choice_Callback(~, ~, handles)

set(handles.extract_text, 'String', 'Payload not extracted');

% --- Executes during object creation, after setting all properties.
function drand_choice_CreateFcn(hObject, ~, ~)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function dperm_choice_Callback(~, ~, handles)

set(handles.extract_text, 'String', 'Payload not extracted');

% --- Executes during object creation, after setting all properties.
function dperm_choice_CreateFcn(hObject, ~, ~)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%% ---------- PAYLOAD EXTRACTION FROM IMAGE BUTTON ----------
function extractImg_button_Callback(~, ~, handles)

X = getappdata(handles.extractImg_button, 'TamperedImage');
a = str2double(get(handles.drand_choice,'String'));
b = str2double(get(handles.dperm_choice,'String'));
if isnan(a) || isnan(b) || a > intmax('uint32') || b > intmax('uint32') || a < 0 || b < 0
    msgbox('Extracting values are not proper!', 'Error','error'); 
else 
    rand_seed = int32(a);
    perm_seed = int32(b);
    d = get(handles.dqim_choice, 'String');
    v = get(handles.dqim_choice, 'Value');
    delta = str2double(d(v));

    E = payload_extraction(X,delta);
    [XOR, D] = payload_decryption(E,perm_seed,rand_seed);

    C1 = {E, XOR, D};
    setappdata(handles.showRecPay_button,'PayloadExtractionProcess',C1);
    setappdata(handles.SaveExtPayload_button,'ExtPayload', D);
    C2 = {X, D};
    setappdata(handles.compareImg_button,'ExtractedPayload',C2);
    
    set(handles.SaveExtPayload_button,'Enable','on'); 
    set(handles.showRecPay_button,'Enable','on'); 
    set(handles.compareImg_button,'Enable','on'); 
    set(handles.saveImage_button,'Enable','on'); 
    set(handles.extract_text, 'String', 'Payload extracted');
end

%% ---------- SHOW PAYLOAD EXTRACTION PROCESS BUTTON ----------
function showRecPay_button_Callback(~, ~, handles)

C = getappdata(handles.showRecPay_button,'PayloadExtractionProcess');
E = C{1,1};
XOR = C{1,2};
P_dec = C{1,3};

figure('ToolBar','figure');
colormap gray;
subplot(221)
imagesc(E); title('Extracted payload'); axis equal; axis tight;
subplot(222)
imagesc(XOR); title('Payload after XOR-ing'); axis equal; axis tight;
subplot(223)
imagesc(P_dec); title('Inverse permutation of XOR-ed payload'); axis equal; axis tight;


%% ---------- SHOW COMPARASION OF IMAGES BUTTON ----------
function compareImg_button_Callback(~, ~, handles)
C = getappdata(handles.compareImg_button,'ExtractedPayload');
Y = C{1,1}; 
D = C{1,2};

S = size(Y);
%C1 = getappdata(handles.compareImg_button ,'OriginalImage');
%if isempty(C1)
%    msgbox('Watermarked image is not loaded!', 'Error','error'); 
%else
%    X = C1{1,1};
%    S = C1{1,2};

R = image_reconstruction(D,S);

setappdata(handles.saveImage_button,'ReconstructedImage',R);

[PSNR] = measerr(Y,R);

figure('ToolBar','figure');
colormap gray;
subplot(121)
imagesc(Y); title('Tampered image'); axis equal; axis tight; colormap gray;
subplot(122)
imagesc(R); title('Reconstructed image from extracted payload'); axis equal; axis tight; colormap gray;

PSNRBox = uicontrol('style','text');
PSNRBoxvalue = uicontrol('style','text');
set(PSNRBox,'String','PSNR: ');
set(PSNRBoxvalue,'String',PSNR);
set(PSNRBox,'Position',[200,30,50,20]);
set(PSNRBoxvalue,'Position',[250,30,50,20]);


%% ---------- SAVE RECONSTRUCTED IMAGE BUTTON ----------
function saveImage_button_Callback(~, ~, handles)

S = getappdata(handles.saveImage_button,'ReconstructedImage');

[FileName,PathName,FilterIndex] = uiputfile({'*.png';'*.jpg'},'Save reconstructed file');
if FilterIndex == 1
     imwrite(S,[PathName FileName],'png');
elseif FilterIndex == 2
     imwrite(S,[PathName FileName],'jpg','Mode','lossless');
end


%% ---------- SAVE EXTRACTED PAYLOAD BUTTON ----------
function SaveExtPayload_button_Callback(hObject, eventdata, handles)

S = getappdata(handles.SaveExtPayload_button,'ExtPayload');

[FileName,PathName,FilterIndex] = uiputfile({'*.png';'*.jpg'},'Save extracted payload');
if FilterIndex == 1
     imwrite(S,[PathName FileName],'png');
elseif FilterIndex == 2
     imwrite(S,[PathName FileName],'jpg','Mode','lossless');
end




