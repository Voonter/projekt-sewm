function [ watermarked_image ] = payload_embedding( image, encpayload, delta)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

[cA,cH,cV,cD] = dwt2(image,'haar','mode','sym');

sx = size(image);
s = size(cA);

MAX = max(cA(:));
zeros=0:2*delta:MAX+delta;
ones=delta:2*delta:MAX;

for i=1:s(1)
    for j=1:s(2)
        if encpayload(i,j) == 1 
            [~,I]=min(abs(cA(i,j)-ones));
            cA(i,j)=ones(I);
        end
        
        if encpayload(i,j) == 0
            [~,I]=min(abs(cA(i,j)-zeros));
            cA(i,j)=zeros(I);
        end
    end
end

watermarked_image = idwt2(cA,cH,cV,cD,'haar',sx);
watermarked_image=uint8(watermarked_image);


end

