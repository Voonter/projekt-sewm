function [XOR, dec_payload ] = payload_decryption( extpayload, permkey, randkey )
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here

s = size(extpayload);

rng(randkey,'twister'); %Inicjalizuje seed generatora
Rand = randi([0 1],s(1),s(2),'uint8'); %wygenerowana maciez losowych watrosci
Rand = logical(Rand);

XOR = bitxor(Rand, extpayload); 


rng(permkey,'twister'); %Inicjalizuje seed generatora dla permutacji
V_per_hor = randperm(s(2));

rng(permkey,'twister');
V_per_ver = randperm(s(1));

for i=1:s(2)
    for j=1:s(1)
     eP_ipermute_hor(V_per_ver(1,j),i) = XOR(j,i);
    end
end


for i=1:s(1)
    for j=1:s(2)
        dec_payload(i,V_per_hor(1,j)) = eP_ipermute_hor(i,j);
    end
end

end

