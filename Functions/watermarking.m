function [ Y ] = watermarking( X , seed, perm_seed,scalar,qim)

step = 8;   % skok
N = size(X,1);
M = size(X,2);


X2 = double(X);
X = double(X);
Y = zeros(N,M);

RandX = GenerateRandX(seed);

for i=1:step:N
    for j=1:step:M
        Y(i:i+step-1,j:j+step-1) = dct2(X(i:i+step-1,j:j+step-1));
    end
end  

tempX = zeros(4,1);
%% Dodaj per lokalizacji średnich localisation = coefficients_location(perm_seed,n,m);
[ X_loc Y_loc ] = coefficients_location(perm_seed,N/step,M/step);

for i=1:step:N
    for j=1:step:M
        tempX(1) = mean2(X2(i:i+step/2-1,j:j+step/2-1)) * scalar;
        tempX(2) = mean2(X2(i:i+step/2-1,j+step/2:j+step-1)) * scalar;
        tempX(3) = mean2(X2(i+step/2:i+step-1,j:j+step/2-1)) * scalar;
        tempX(4) = mean2(X2(i+step/2:i+step-1,j+step/2:j+step-1)) * scalar;
        q = 1;
        for k = 1:step
            for l = 1:step
                 if RandX(k,l) == 2
					 posX = (X_loc(floor(i/step)+1) - 1) * step + k;
                     posY = (Y_loc(floor(j/step)+1) - 1) * step + l;
					 Y(posX,posY) = tempX(q);
                     q = q + 1;              
                 elseif RandX(k,l) == 1
                    u = mod(Y(i+k-1,j+l-1),2*qim);
                    if u <= qim
                        Y(i+k-1,j+l-1) = Y(i+k-1,j+l-1) - u + qim;
                    else
                        Y(i+k-1,j+l-1) = Y(i+k-1,j+l-1) + qim - u;
                    end
                 else 
                    u = mod(Y(i+k-1,j+l-1),2*qim);
                    if u <= qim
                        Y(i + k - 1, j + l - 1) = Y(i+k-1,j+l-1) - u;
                    else
                        Y(i+k-1,j+l-1) = Y(i+k-1,j+l-1) + 2*qim - u;
                    end
                 end
            end
        end
    end
end


for i=1:step:N
    for j=1:step:M
        X(i:i+step-1,j:j+step-1) = idct2(Y(i:i+step-1,j:j+step-1));
    end
end

Y = uint8(X);

end




