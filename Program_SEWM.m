function varargout = Program_SEWM(varargin)
% PROGRAM_SEWM MATLAB code for Program_SEWM.fig
%      PROGRAM_SEWM, by itself, creates a new PROGRAM_SEWM or raises the existing
%      singleton*.
%
%      H = PROGRAM_SEWM returns the handle to a new PROGRAM_SEWM or the handle to
%      the existing singleton*.
%
%      PROGRAM_SEWM('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PROGRAM_SEWM.M with the given input arguments.
%
%      PROGRAM_SEWM('Property','Value',...) creates a new PROGRAM_SEWM or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Program_SEWM_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Program_SEWM_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Program_SEWM

% Last Modified by GUIDE v2.5 17-Jun-2014 22:17:18

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Program_SEWM_OpeningFcn, ...
                   'gui_OutputFcn',  @Program_SEWM_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Program_SEWM is made visible.
function Program_SEWM_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Program_SEWM (see VARARGIN)

% Choose default command line output for Program_SEWM
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Program_SEWM wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Program_SEWM_OutputFcn(~, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
addpath(genpath('./Functions'));




% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(~, ~, ~)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


varargout = Abbas_Cheddad_algorithm(figure(Abbas_Cheddad_algorithm));
%dodac obsluge zamkniecia


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(~, ~, ~)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

varargout = Second_algorithm(figure(Second_algorithm));


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, ~, ~)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
selection = questdlg('Are you sure you want to close the program?','Close Request','Yes','No','Yes'); 

switch selection
    case 'Yes',
        close(findobj('name','Abbas_Cheddad_algorithm'));
        close(findobj('name','Second_algorithm'));
        delete(hObject);
    case 'No'
        return
end 




